import { SessionService } from './../../services/session/session.service';
import { async } from '@angular/core/testing';
import { AuthService } from './../../services/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
registerForm: FormGroup = new FormGroup({
  username: new FormControl('', [Validators.required, Validators.minLength(3)]),
  password: new FormControl('', [Validators.required, Validators.minLength(4)])
})

  isLoading: boolean = false;
  registerError: string;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) { 
  if (this.session.get() !== false ) {
    //redirect to Dashboard
    this.router.navigateByUrl('/dashboard');
  }
}
  ngOnInit(): void {
  }
  get username(){
    return this.registerForm.get('username');
  }
  get password(){
    return this.registerForm.get('password');
  }



 async onRegisterClicked() {
   this.registerError = '';
   try {
     this.isLoading = true;
    const result: any = await this.auth.register(this.registerForm.value);
    if (result.status < 400) {
      this.session.save({
        token: result.data.token,
        username: result.data.user.username
      });
 
    
      this.router.navigateByUrl('/dashboard');   
    }


   } catch (e) {
     console.error(e.error);
     this.registerError = e.error.error;
         
   }finally {
   this.isLoading = false;
  }
}
}
