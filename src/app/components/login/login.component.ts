import { AuthService } from './../../services/auth/auth.service';
import { SessionService } from './../../services/session/session.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

loginForm: FormGroup = new FormGroup({
  username: new FormControl('', [ Validators.required, Validators.minLength(3) ]),
  password: new FormControl('', [  Validators.required, Validators.minLength(4)])
})
  
  isLoading: boolean = false;
  loginError: string;
  constructor(private session: SessionService, private router: Router, private auth: AuthService) { 
    if (this.session.get() !== false) {
      //redirect to Dashboard
      this.router.navigateByUrl('/dashboard');
    }
  }

  ngOnInit(): void {
  }
  get username() {
    return this.loginForm.get('username')
  }
  get password() {
    return this.loginForm.get('password')
  }

 async onLoginClicked() {
    try {
      this.isLoading = true;
      const result: any = await this.auth.login( this.loginForm.value );
      if (result.status < 400) {
        this.session.save( { token: result.data.token, username: result.data.user.username } );
        this.router.navigateByUrl('/dashboard')
      }

    }catch (e){
      this.loginError = e.error.error;

    }finally {
      this.isLoading = false;
    }
    
  }

}
